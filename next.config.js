const withCss = require('@zeit/next-css');
const withSass = require('@zeit/next-sass');
const withImages = require('next-images');
const compose = require('next-compose');

module.exports = compose([
    withSass(),
    withImages(),
    withCss({
        cssModules: true
    })
]);


