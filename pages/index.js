import React, { Component } from 'react';
import Head from 'next/head';
import { initGA, logPageView } from "../components/googleanalytics";
import { Link, animateScroll as scroll } from "react-scroll";
import "../styles/styles.scss";
import Card from "../components/card";
import Overview from "../components/overview";
import Charts from "../components/charts";
import World from "../components/world";
import Footer from "../components/footer";
import {formatN} from "../Utils";
import { FacebookProvider, Like, ShareButton } from 'react-facebook';



class Index extends React.Component {
    constructor() {
        super();
        this.state = {
            globalStatus: 'Pandemia',
            lastUpdate: '',
            yesterday: '',
            andamentoNazionale: '',
            andamentoNazionaleDayBefore: '',
            totaleCasi: '',
            positivi: '',
            positiviDiff: '',
            guariti: '',
            guaritiDiff: '',
            deceduti: '',
            decedutiDiff: '',
            tamponi: '',
            terapiaIntensiva: '',
            ricoveratiSintomi: '',
            isolamentoDomiciliare: '',
            tableHead: true
        };

    }


    isToday() {
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth()+1;
        const yyyy = today.getFullYear();
        if(dd<10)
        {
            dd=`0${dd}`;
        }
        if(mm<10)
        {
            mm=`0${mm}`;
        }
        today = `${yyyy}-${mm}-${dd}`;

        if (today == this.state.lastUpdate.substr(0, 10)) {
            return true
        } else {
            return false
        }
    }


    componentDidMount() {

        const localUrl = location.hostname

        if (localUrl !== 'localhost') {

            if (!window.GA_INITIALIZED) {
                initGA()
                window.GA_INITIALIZED = true
            }
            logPageView()

        }


        fetch('https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-andamento-nazionale.json')
            .then(res => res.json())
            .then((data) => {
                const lastData = data.length - 1
                const yesterdayData = data.slice(-2)





                this.setState({
                    andamentoNazionale: data[lastData],
                    andamentoNazionaleDayBefore: yesterdayData,
                    lastUpdate: data[lastData].data,
                    totaleCasi: data[lastData]['totale_casi'],
                    positivi: data[lastData]['totale_positivi'],
                    nuoviPositivi: data[lastData]['variazione_totale_positivi'],
                    positiviDiff: yesterdayData[1]['totale_positivi'] - yesterdayData[0]['totale_positivi'],
                    guariti: data[lastData]['dimessi_guariti'],
                    guaritiDiff: yesterdayData[1]['dimessi_guariti'] - yesterdayData[0]['dimessi_guariti'],
                    deceduti: data[lastData]['deceduti'],
                    decedutiDiff: yesterdayData[1]['deceduti'] - yesterdayData[0]['deceduti'],
                    tamponi: data[lastData]['tamponi'],
                    terapiaIntensiva: data[lastData]['terapia_intensiva'],
                    ricoveratiSintomi: data[lastData]['ricoverati_con_sintomi'],
                    isolamentoDomiciliare: data[lastData]['isolamento_domiciliare'],
                    yesterdayDate: yesterdayData[0].data.slice(0, 10)
                })
                console.log();
            })
            .catch(console.log)


        const d = new Date(Date.now() - 864e5);
        const dtf = new Intl.DateTimeFormat('en', { year: 'numeric', month: '2-digit', day: '2-digit' })
        const [{ value: mm },,{ value: dd },,{ value: yy }] = dtf.formatToParts(d)

        const yesterday = `${yy}-${mm}-${dd}`

        this.setState({
            yesterday: yesterday
        })
    }





    render() {

        let lastUpdate
        if (this.isToday()) {
             lastUpdate = 'Oggi alle ' + this.state.lastUpdate.substr(this.state.lastUpdate.length - 8).slice(0,-3);
        } else if (this.state.yesterday === this.state.yesterdayDate || this.state.yesterday != this.state.yesterdayDate ) {
            lastUpdate = 'Ieri alle ' + this.state.lastUpdate.substr(this.state.lastUpdate.length - 8).slice(0,-3);
        } else {
             lastUpdate = this.state.lastUpdate.slice(0,-3)
        }


        return (
            <div>
                <Head>
                    <title>Daily Covid-19</title>
                    <meta name="description" content="L'aggiornamento quotidiano sulla situazione Covid-19 in Italia e nel Mondo" />
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                    <meta name="author" content="Claudio Sabatinelli"/>

                    <meta property="og:title" content="Daily Covid-19"/>
                    <meta property="og:description" content="L'aggiornamento quotidiano sulla situazione Covid-19 in Italia e nel Mondo"/>
                    <meta property="og:image" content="https://dailycovid-19.it/images/fb.jpg"/>
                    <meta property="og:url" content="https://dailycovid-19.it/"/>
                    <meta property="og:site_name" content="Daily Covid-19"/>

                    <meta name="twitter:title" content="Daily Covid-19"/>
                    <meta name="twitter:description" content="L'aggiornamento quotidiano sulla situazione Covid-19 in Italia e nel Mondo"/>
                    <meta name="twitter:image" content="https://dailycovid-19.it/images/fb.jpg"/>
                    <meta name="twitter:card" content="summary_large_image"/>

                    <meta name="google-site-verification" content="kWLt5KfJS_jzKv94OI7unY-EFDiFnrcxw9zatPDcAaY" />

                </Head>
                <section className="hero">
                    <div className="container">
                        <div className="col col--5 fl-l">
                            <div className="covid">
                                <h1>COVID-19</h1>
                                <ul className="covid__status">
                                    <li>
                                        <strong>Stato di emergenza: </strong>
                                        <span className="status">{this.state.globalStatus}</span>
                                    </li>
                                    <li>
                                        <strong>Ultimo aggiornamento: </strong>
                                        <span> {lastUpdate}</span>
                                    </li>
                                    <li>
                                        <strong>Casi totali: </strong>
                                        <span>{formatN(this.state.totaleCasi)}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col col--5 fl-r">
                            <div className="main-data">
                                <Card
                                    type="main-data"
                                    title="Attualmente positivi"
                                    content={formatN(this.state.positivi)}
                                    diff={formatN(this.state.positiviDiff) > 0 ? '+ ' + formatN(this.state.positiviDiff) : '- ' + formatN(this.state.positiviDiff).slice(1)}
                                ></Card>
                                <Card
                                    type="main-data"
                                    title="Guariti"
                                    content={formatN(this.state.guariti)}
                                    diff={'+ ' + formatN(this.state.guaritiDiff)}
                                ></Card>
                                <Card
                                    type="main-data"
                                    title="Deceduti"
                                    content={formatN(this.state.deceduti)}
                                    diff={'+ ' + formatN(this.state.decedutiDiff)}
                                ></Card>
                            </div>
                        </div>
                        <div className="overview">
                            <ul>
                                <li>
                                    <strong>Tamponi</strong>
                                    <span>{formatN(this.state.tamponi)}</span>
                                </li>
                                <li>
                                    <strong>Terapia <br/>intensiva</strong>
                                    <span>{formatN(this.state.terapiaIntensiva)}</span>
                                </li>
                                <li>
                                    <strong>Ricoverati <br/>con sintomi</strong>
                                    <span>{formatN(this.state.ricoveratiSintomi)}</span>
                                </li>
                                <li>
                                    <strong>Isolamento <br/>domiciliare</strong>
                                    <span>{formatN(this.state.isolamentoDomiciliare)}</span>
                                </li>
                            </ul>
                        </div>
                        <div className="btn">
                            <Link
                                activeClass="active"
                                to="overviews"
                                spy={true}
                                smooth={true}
                                offset={-10}
                                duration= {600}
                            >Scopri di più<i className='ion-chevron-down'></i></Link>
                        </div>
                    </div>
                </section>

                <main>
                    <div className="container">
                            <Overview/>
                        <section className="charts">
                            <Charts/>
                        </section>
                    </div>
                    <section className="world">
                        <World/>
                    </section>
                </main>
                <div className="social-wrapper">
                    <FacebookProvider appId="232084951241926" language='it_IT'>
                        <Like href="http://dailycovid-19.it/" colorScheme="dark" layout="button_count" data-size="small" showFaces share />
                    </FacebookProvider>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default Index


