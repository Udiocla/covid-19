import "../styles/styles.scss";
import ListItem from '../components/listItem'
import Sticky from 'react-sticky-state';
import {Link} from "react-scroll";
import React from "react";
import {formatN} from "../Utils";


class Overview extends React.Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            lastUpdate: '',
            sortedRegions: [],
            sortedProv: [],
            activeClass: 'overview__list-head',
            overviewHead: false,
            overviewList: false,
            overviewListLast: false,
            activeItem: false,
            active: false,
            activeID: null,
            activeLbl: false
        }
        this.toggleClass = this.toggleClass.bind(this);
    }



    toggleClass = (e) => {
        console.log(e.currentTarget);
        this.setState({
            active: !this.state.active,
            activeID: e,
            activeLbl: !this.state.activeLbl
        })
    }



    overviewHeadFixed = () => {
        const overviewHead = this.overviewHeadRef.getBoundingClientRect();
        const overviewList = this.overviewListRef.getBoundingClientRect();
        const overviewListLast = this.overviewListRef.getBoundingClientRect();

        this.setState({
            overviewHead: overviewHead.y <= 0 ? true : false,
            overviewList: overviewList.y,
            overviewListLast: overviewListLast.y > -(overviewList.height - 60) ? true : false
        });
    };


    componentDidMount() {
        this._isMounted = true;

        fetch('https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni.json')
            .then(res => res.json())
            .then((data) => {

                let lastData = data.slice(Math.max(data.length - 21, 0))

                var bolzano = lastData.slice(2, 3)
                var trento = lastData.slice(17, 18)

                var trentino = {};
                var trentinoCombined = {
                    data: trento[0].data,
                    codice_regione: 4,
                    denominazione_regione: 'Trentino Alto Adige',
                    totale_casi: (trento[0].totale_casi + bolzano[0].totale_casi),
                    totale_positivi: (trento[0].totale_positivi + bolzano[0].totale_positivi),
                    dimessi_guariti: (trento[0].dimessi_guariti + bolzano[0].dimessi_guariti),
                    deceduti: (trento[0].deceduti + bolzano[0].deceduti),
                    tamponi: (trento[0].tamponi + bolzano[0].tamponi),
                    terapia_intensiva: (trento[0].terapia_intensiva + bolzano[0].terapia_intensiva),
                    ricoverati_con_sintomi: (trento[0].ricoverati_con_sintomi + bolzano[0].ricoverati_con_sintomi),
                    isolamento_domiciliare: (trento[0].isolamento_domiciliare + bolzano[0].isolamento_domiciliare),
                };
                trentino = {...trentino, ...trentinoCombined};

                lastData.splice(17, 1);
                lastData.splice(2, 1);


                lastData.push(trentino)

                //Ordino per totale casi
                lastData.sort(function (a, b) {
                    return b.totale_positivi - a.totale_positivi
                })


                if (this._isMounted) {
                    this.setState({
                        sortedRegions: lastData,
                        lastUpdate: lastData[0].data
                    })
                }
            })
            .catch(console.log)

        fetch('https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-province.json')
            .then(res => res.json())
            .then((data) => {
                const lastData = data.filter(obj => {
                    return obj.data === this.state.lastUpdate
                })
                if (this._isMounted) {
                    this.setState({
                        sortedProv: lastData
                    })
                }

            })
            .catch(console.log)

        window.addEventListener("scroll", this.overviewHeadFixed);

    }

    componentWillUnmount() {
        this._isMounted = false;
    }


    getProv (region) {
        const sortedProv = this.state.sortedProv
        let prov

        sortedProv.sort(function (a, b) {
            return b.totale_casi - a.totale_casi
        })

        prov = sortedProv.map((obj, i) => {
            if (obj.codice_regione === region.codice_regione) {

                let provincia
                if (obj.denominazione_provincia == 'In fase di definizione/aggiornamento') {
                    provincia = <i>In definizione</i>
                } else {
                    provincia = obj.denominazione_provincia
                }

                return (
                    <li key={i}>
                        <strong>{provincia}</strong>
                        <span>{formatN(obj.totale_casi)}</span>
                    </li>
                )
            }
        })
        return prov
    }


    render() {
        return (
            <section className={this.state.activeLbl ? "overviews overviews--hidden-lbl" : "overviews"}>
                <div
                    className={this.state.overviewList < 100 && this.state.overviewListLast ? 'overview__list-head fixed' : 'overview__list-head'}
                    ref={node => (this.overviewHeadRef = node)}
                >
                    <ul className='col col--4 fl-l'>
                        <li><strong>Regione</strong></li>
                        <span className='provLbl'><strong>Provincia | <span>Casi totali</span></strong></span>
                        <li><i className="ion-medkit"></i></li>
                        <li><i className="ion-android-happy"></i></li>
                        <li><i className="ion-android-sad"></i></li>
                    </ul>
                    <ul className='col col--6 ta-r fl-r d-only'>
                        <li><strong>Tamponi</strong></li>
                        <li><strong>Terapia <br/>intensiva</strong></li>
                        <li><strong>Ricoverati <br/>con sintomi</strong></li>
                        <li><strong>Isolamento <br/>domiciliare</strong></li>
                    </ul>
                </div>

                <div className="overview__list" ref={node => (this.overviewListRef = node)}>
                    {this.state.sortedRegions.map((region, index) => (
                        <ListItem
                            key={index}
                            dataKey={index}
                            className={this.state.activeID === index && this.state.active ? 'overview__list-item active' : 'overview__list-item'}
                            regione={region.denominazione_regione}
                            totale_casi={region.totale_casi}
                            positivi={region.totale_positivi}
                            dimessi_guariti={region.dimessi_guariti}
                            deceduti={region.deceduti}
                            tamponi={region.tamponi}
                            terapia_intensiva={region.terapia_intensiva}
                            ricoverati_con_sintomi={region.ricoverati_con_sintomi}
                            isolamento_domiciliare={region.isolamento_domiciliare}
                            province={this.getProv(region)}
                            onClick={this.toggleClass.bind(this, index)}
                        ></ListItem>
                    ))}
                    <div className="source">
                        Fonte: <a href="https://github.com/pcm-dpc/COVID-19/" target='_blank'>Dipartimento della Protezione Civile</a>
                    </div>

                </div>
            </section>
        )
    }
}


export default Overview