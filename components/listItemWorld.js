import "../styles/styles.scss";
import React, {Component} from 'react';
import {formatN} from "../Utils";

class ListItemWorld extends Component {
    constructor(props) {
        super(props);
    }



    render() {


        return (
                <div className={this.props.className}>
                    <ul className='flw'>
                        <li>
                            <span>
                                {this.props.name}
                            </span>
                            <span>
                                <strong className='m-only'>
                                    {formatN(this.props.totali)}
                                </strong>
                            </span>

                            <ul>
                                <li className='totali d-only'>{formatN(this.props.totali)}</li>
                                <li className='positivi'>{formatN(this.props.positivi)}</li>
                                <li className='guariti'>{formatN(this.props.dimessi_guariti)}</li>
                                <li className='deceduti'>{formatN(this.props.deceduti)}</li>
                            </ul>
                        </li>
                    </ul>
                </div>
        )
    }
};

export default ListItemWorld
