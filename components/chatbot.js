const { containerBootstrap } = require('@nlpjs/core');
const { Nlp } = require('@nlpjs/nlp');
const { LangEn } = require('@nlpjs/lang-en-min');


class Chatbot extends React.Component {
    constructor() {
        super();
        this.state = {
            answer: ''
        };
    }

    componentDidMount() {
        (async () => {
            const container = await containerBootstrap();
            container.use(Nlp);
            container.use(LangEn);
            const nlp = container.get('nlp');
            nlp.settings.autoSave = false;
            nlp.addLanguage('it');
            // Adds the utterances and intents for the NLP
            nlp.addDocument('it', 'goodbye for now', 'greetings.bye');
            nlp.addDocument('it', 'bye bye take care', 'greetings.bye');
            nlp.addDocument('it', 'okay see you later', 'greetings.bye');
            nlp.addDocument('it', 'bye for now', 'greetings.bye');
            nlp.addDocument('it', 'i must go', 'greetings.bye');
            nlp.addDocument('it', 'hello', 'greetings.hello');
            nlp.addDocument('it', 'hi', 'greetings.hello');
            nlp.addDocument('it', 'howdy', 'greetings.hello');

            // Train also the NLG
            nlp.addAnswer('it', 'greetings.bye', 'Till next time');
            nlp.addAnswer('it', 'greetings.bye', 'see you soon!');
            nlp.addAnswer('it', 'greetings.hello', 'Hey there!');
            nlp.addAnswer('it', 'greetings.hello', 'Greetings!');
            await nlp.train();
            const response = await nlp.process('it', 'goodbye');
            console.log(response.answer);
            this.setState ({
                answer : response.answer
            })
        })();
    }

    render() {
        return (
            <div>
                {this.state.answer}
            </div>
        )
    }

}

export default Chatbot;