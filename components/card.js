import "../styles/styles.scss";

const Card = props => (
    <div className="card" type={props.type}>
        <div className='card__title'>{props.title}</div>
        <div className='card__content'>{props.content}<span> {props.diff} <small>(24h)</small></span></div>
    </div>
);

export default Card