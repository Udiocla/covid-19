import "../styles/styles.scss";

const Footer = () => (
    <footer className="footer">
        <div className="container">
            <div className="copy">Made with &#9829; and Amuchina <br/>
            <small>by <a href="https://www.instagram.com/udiocla/" target='_blank'> Claudio Sabatinelli</a></small></div>
        </div>
    </footer>
);

export default Footer;