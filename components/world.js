import "../styles/styles.scss";
import ListItemWorld from '../components/listItemWorld';
import React, {Component} from 'react';
import {formatN} from "../Utils";
import Moment from '../node_modules/react-moment';
import 'moment-timezone';

Moment.globalLocale = 'it';
Moment.globalTimezone = 'Europe/Rome';
Moment.globalFormat = 'D MMMM HH:mm';



class World extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sortedWorldCountries : [],
            lastUpdate: '',
            confirmed: '',
            recovered: '',
            deaths: ''
        }
    }



    componentDidMount() {
        fetch('https://raw.githubusercontent.com/enrichman/covid19/master/world/data.json')
            .then(res => res.json())
            .then((data) => {
                if (location.hostname == 'localhost') {
                    console.log(data);
                }
                let sortedWorldCountries
                sortedWorldCountries = data.countries.sort(function (a, b) {
                    return b.confirmed - a.confirmed
                })

                sortedWorldCountries = sortedWorldCountries.slice(0, 10)

                this.setState({
                    sortedWorldCountries: sortedWorldCountries,
                    lastUpdate: <Moment add={{ days: 1 }}>{data.last_update}</Moment>,
                    confirmed: data.confirmed,
                    recovered: data.recovered,
                    deaths: data.deaths
                })
            })
            .catch(console.log)
    }

    translateCountry(country) {
        switch(country) {
            case 'US':
                return 'Stati Uniti';
            case 'Spain':
                return 'Spagna';
            case 'Italy':
                return 'Italia';
            case 'France':
                return 'Francia';
            case 'Germany':
                return 'Germania';
            case 'United Kingdom':
                return 'Regno Unito';
            case 'China':
                return 'Cina';
            case 'Iran':
                return 'Iran';
            case 'Turkey':
                return 'Turchia';
            case 'Belgium':
                return 'Belgio';
            case 'Netherlands':
                return 'Olanda';
            case 'Switzerland':
                return 'Svizzera';
            case 'Brazil':
                return 'Brasile';
            case 'Portugal':
                return 'Portogallo';
            case 'Israel':
                return 'Israele';
            case 'Ireland':
                return 'Irlanda';
            case 'Sweden':
                return 'Svezia';
            case 'South Africa':
                return 'Sud Africa';
            case 'Peru':
                return 'Perù';
            case 'Chile':
                return 'Cile';
            case 'Mexico':
                return 'Messico';
            case 'Spain':
                return 'Spagna';
            case 'Saudi Arabia':
                return 'Arabia Saudita';
            case 'Turkey':
                return 'Turchia';
            case 'Italy':
                return 'Italia';
            case 'Germany':
                return 'Germania';
            default:
                return country;
        }
    }


    render() {

        return (
            <React.Fragment>
                    <div className="container">
                        <div className="col col--5 fl-l">
                            <div className="section-title section-title--big">
                                <h2>La situazione <br/>nel mondo</h2>
                                <ul className="covid__status">
                                    <li><strong>Ultimo aggiornamento: </strong><span>{this.state.lastUpdate} <small>(Ora italiana)</small></span></li>
                                    <li><strong>Casi totali: </strong><span>{formatN(this.state.confirmed)}</span></li>
                                    <li><strong>Guariti: </strong><span>{formatN(this.state.recovered)}</span></li>
                                    <li><strong>Deceduti: </strong><span>{formatN(this.state.deaths)}</span></li>
                                </ul>
                            </div>
                        </div>
                        <div className='overview-list-world__wrapper'>
                            <div className="section-title section-title">
                                <h2>I primi 10 Paesi <br className='m-only' />per numero di casi totali</h2>
                            </div>
                            <div className='overview-list-world__list-head'>
                                <ul className='col col--3'>
                                    <li className='country'>
                                        Paese
                                        <br/>
                                        <strong className="m-only">
                                            Casi totali
                                        </strong>
                                    </li>
                                </ul>
                                <ul className='col col--7 ta-c'>
                                    <li className='d-only'><strong>Casi totali</strong></li>
                                    <li><i className="ion-medkit"></i></li>
                                    <li><i className="ion-android-happy"></i></li>
                                    <li><i className="ion-android-sad"></i></li>
                                </ul>
                            </div>
                            {this.state.sortedWorldCountries.map((country, index) => (
                                <ListItemWorld
                                    key={index}
                                    dataKey={index}
                                    className={'overview-list-world__list-item'}
                                    name={this.translateCountry(country.name)}
                                    totali={country.confirmed}
                                    positivi={country.confirmed - (country.recovered + country.deaths)}
                                    dimessi_guariti={country.recovered}
                                    deceduti={country.deaths}
                                ></ListItemWorld>
                            ))}
                            <div className="source">
                                Fonte: <a href="https://systems.jhu.edu/research/public-health/ncov/" target='_blank'>Center for Systems Science and Engineering (CSSE) at Johns Hopkins University (JHU)</a>
                            </div>
                        </div>
                    </div>
            </React.Fragment>
        )
    }


}

export default World