import React, { PureComponent } from 'react';
import {
    LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer,
    AreaChart, ReferenceLine, Area, BarChart, Bar, Cell
} from 'recharts';

import {formatN} from "../Utils";

class Charts extends React.Component {
    constructor() {
        super();
        this.state = {
            sortedRegions: []
        };

    }

    componentDidMount() {
        fetch('https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-andamento-nazionale.json')
            .then(res => res.json())
            .then((data) => {
                const allData = data
                this.setState({
                    sortedRegions: data
                })

            })
            .catch(console.log)
    }

    render() {
        const items = [];
        const tamponiPositivi = [];

        this.state.sortedRegions.forEach((item, index)=> {

            let month = item.data.slice(5, 7)
            if (month === '03') {
                month = 'Marzo'
            } else if (month === '04'){
                month = 'Aprile'
            } else if (month === '05'){
                month = 'Maggio'
            } else if (month === '06'){
                month = 'Giugno'
            } else if (month === '07'){
                month = 'Luglio'
            } else if (month === '08'){
                month = 'Agosto'
            } else if (month === '09'){
                month = 'Settembre'
            } else if (month === '10'){
                month = 'Ottobre'
            } else {
                month = month
            }
            let day = item.data.slice(8, 10)

            items.push({
                    title: day + '\n' + month,
                    guariti: formatN(item.dimessi_guariti),
                    positivi: formatN(item.totale_positivi),
                    deceduti: formatN(item.deceduti),
                    totale: formatN(item.totale_casi),
                    amt: parseInt(item.totale_casi)
            })


            /*let tamponiDiff = []
            let positiviDiff = []
            if (index > 0 && index < this.state.sortedRegions.length) {
                tamponiDiff = (item.tamponi - this.state.sortedRegions[index -1].tamponi)
                positiviDiff = (item.totale_positivi - this.state.sortedRegions[index -1].totale_positivi)

                tamponiPositivi.push({
                    'Tamponi': tamponiDiff,
                    'Nuovi positivi': positiviDiff,
                    amt: formatN(parseInt(tamponiDiff - positiviDiff))
                })
            }*/

        })



        return (
            <div style={{ width: '100%', height: '600px' }}>
                <ResponsiveContainer width="100%" height="100%">

                    <AreaChart data={items.slice(Math.max(items.length - 14, 0))}>

                        <defs>
                            <linearGradient id="totale" x1="2" y1="1" x2="0" y2="1">
                                <stop offset="5%" stopColor="#8b8f92" stopOpacity={0.8}/>
                                <stop offset="95%" stopColor="#8b8f92" stopOpacity={0.3}/>
                            </linearGradient>

                            <linearGradient id="positivi" x1="0" y1="0" x2="0" y2="1">
                                <stop offset="5%" stopColor="#f89406" stopOpacity={0.8}/>
                                <stop offset="95%" stopColor="#f89406" stopOpacity={0.3}/>
                            </linearGradient>
                        </defs>

                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="title" />
                        <YAxis
                            tickFormatter={tick => {
                                return tick.toLocaleString();
                            }}
                        />
                        <Tooltip
                        />
                        <Legend />
                        <Area type="monotone" dataKey="totale" name='Casi totali' stroke="#8b8f92" fill="url(#totale)" />
                        <Area type="monotone" dataKey="positivi" name='Positivi' stroke="#f89406" fill="url(#positivi)"/>
                        <Area type="monotone" dataKey="guariti" name='Guariti' stroke="#00A060" fill="#82ca9d" />
                        <Area type="monotone" dataKey="deceduti" name='Deceduti' stroke="#f03434" fill="#f03434"/>
                    </AreaChart>





                </ResponsiveContainer>
                {/*<ResponsiveContainer width="50%" height="100%">


                    <BarChart
                        width={500}
                        height={300}
                        data={tamponiPositivi.slice(Math.max(items.length - 7, 0))}
                        margin={{
                            top: 5, right: 30, left: 20, bottom: 5,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <ReferenceLine y={0} stroke="#000" />
                        <Bar dataKey="Tamponi" fill="#8884d8" />
                        <Bar dataKey="Nuovi positivi" fill="#f89406" />
                    </BarChart>




                </ResponsiveContainer>*/}

            </div>



        );
    }
}
export default Charts;