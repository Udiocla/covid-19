import "../styles/styles.scss";
import React, {Component} from 'react';
import {formatN} from "../Utils";


class ListItem extends Component {
    constructor(props) {
        super(props);
    }



    render() {

        return (
            <div onClick={this.props.onClick} className={this.props.className}>
                <ul className='col col--4 fl-l'>
                    <li>
                        <span>{this.props.regione}</span>
                        <ul>
                            <li className='positivi'>{formatN(this.props.positivi)}</li>
                            <li className='guariti'>{formatN(this.props.dimessi_guariti)}</li>
                            <li className='deceduti'>{formatN(this.props.deceduti)}</li>
                        </ul>
                    </li>
                </ul>
                <ul className='col col--6 ta-r fl-r'>
                    <li>
                        <strong className='m-only'>Tamponi</strong>
                        <span>{formatN(this.props.tamponi)}</span>
                    </li>
                    <li>
                        <strong className='m-only'>Terapia <br/>intensiva</strong>
                        <span>{formatN(this.props.terapia_intensiva)}</span>
                    </li>
                    <li>
                        <strong className='m-only'>Ricoverati <br/>con sintomi</strong>
                        <span>{formatN(this.props.ricoverati_con_sintomi)}</span>
                    </li>
                    <li>
                        <strong className='m-only'>Isolamento <br/>domiciliare</strong>
                        <span>{formatN(this.props.isolamento_domiciliare)}</span>
                    </li>
                </ul>
                <div className="prov__list">
                    <ul>
                        <li className='m-only mb-20'>
                            <strong>Provincia</strong>
                            <span>Casi totali</span>
                        </li>
                        {this.props.province}
                    </ul>
                </div>
                <div className="prov">
                    <i className='ion-chevron-down'></i>
                </div>
            </div>
        )
    }
};

export default ListItem
